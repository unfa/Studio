# studio

This repository holds tools I've created for my videomaking and livestreaming work using open-source software and Linux.

Hopefully it'll help others do similar things with libre tools!